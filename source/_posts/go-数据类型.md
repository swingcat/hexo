---
title: go数据类型
date: 2018-04-24 17:45:27
tags: go
---
# 指针
Go有指针，存储的是值所对应的地址。

`*T`对应的是T类型数据的指针，空值为`nil`

```go
var p *int
```

`&`是取址符，取出变量的地址

```go
i := 42
p = &i
```

`*`是寻址符，取指针对应的值

```go
fmt.Println(*p)
*p = 21
```

# struct

`struct`是域的集合

```go
type Vertex struct {
    X int
    Y int
}

func main() {
    v := Vertex{1, 2}
    v.X = 4
    fmt.Println(v.X)
}
```

可以通过指针直接引用`struct`内的域

```go
v := Vertex{1, 2}
p := &v
p.X = 1e9
```

```go
type Vertex struct {
    X, Y int
}

var (
    v1 = Vertex{1, 2}
    v2 = Vertex{X: 1}
    v3 = Vertex{}
    p = &Vertex{1, 2}
)

func main() {
    fmt.Println(v1, p, v2, v3)
}
```

# array

声明数组: `var a [10]int`，表示由10个int型组成的数组，数组的长度固定

# slice

`slice`是`array`的一个片段，有更广泛的适用性

```go
primes := [6]int{1,2,3,4,5,6}
var s []int = primes[1:4]
fmt.Println(s)
```

修改`slice`内的值也会改变对应的`array`内的值

`[3]bool{true, false, true}`和`[]bool{true, false, true}`结果是一样的

对于数组`var a [10]int`，这些都是相同的：

```go
a[0:10]
a[:10]
a[0:]
a[:]
```

`slice`的`length`和`capability`通过`len(s)`和`cap(s)`获取

`slice`的空值是`nil`

可以通过`make`函数构建`slice`

```go
a := make([]int, 5) // len(a) == 5
b := make([]int, 0, 5) // len(b) == 0, cap(b) == 5
b = b[:cap(b)] // len(b) == 5, cap(b) == 5
b = b[1:] // len(b) == 4, cap(b) == 4
```

可以用`append`方法为`slice`后面添加元素，当超出`cap(s)`的时候会自动添加

```go
package main

import "fmt"

func main() {
	var s []int
	printSlice(s)

	// append works on nil slices.
	s = append(s, 0)
	printSlice(s)

	// The slice grows as needed.
	s = append(s, 1)
	printSlice(s)

	// We can add more than one element at a time.
	s = append(s, 2, 3, 4)
	printSlice(s)
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
```

# range

`for`循环里的`range`可以在`slice`或者是map里循环

```go
var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}

func main() {
    for i, v := range pow {
        fmt.Printf("2**%d = %d\n", i, v)
    }
}
```

可以用`_`作占位符

```go
func main() {
    pow := make([]int, 10)
    for i := range pow {
        pow[i] = 1 << uint(i)
    }
    for _, v := range pow {
        fmt.Printf("%d\n", v)
    }
}
```

# map

空值为`nil`，用`make`来构建

```go
type Vertex struct {
    Lat, Long float64
}

var m map[string]Vertex

func main() {
    m = make(map[string]Vertex)
    m["bell labs"] = Vertex{
        11.1, 12.1
    }
    fmt.Println(m["bell labs"])
}
```

```go
m[key] = elem // 赋值或者更新
elem := m[key] // 取值
delete(m, key) // 删除
elem, ok := m[key] // 如果m由key，则ok为true；没有则为false，elem为默认值
```

# func

函数也可以当参数

```go
func compute(fn func(float64, float64) float64) float64 {
    return fn(3, 4)
}

func main() {
    hypot := func(x, y float64) float64 {
        return math.Sqrt(x*x + y*y)
    }
    fmt.Println(hypot(5, 12))
    fmt.Println(compute(hypot))
    fmt.Println(compute(math.Pow))
}
```

函数闭包，例如以下`adder`函数，每个通过该函数创建的函数和属于自己的参数`sum`绑定

```go
func adder() func(int) int {
    sum := 0
    return func(x int) int {
        sum += x
        return sum
    }
}

func main() {
    pos, neg := adder(), adder()
    for i := 0; i < 10; i++ {
        fmt.Println(
            pos(i),
            neg(-2*i),
        )
    }
}
```