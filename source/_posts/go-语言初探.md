---
title: go语言初探
date: 2018-04-20 09:55:40
tags: go
---
Go语言出来已经很多年了，本文旨在介绍基础知识点。

### 包

每个Go程序都是由`包`组成的，程序入口就是在`main`包里。

```go
package main

import (
    "fmt"
    "math/rand"
)

func main() {
    fmt.Println("My favorite number is", rand.Intn(10))
}
```

按照规定，包名是import路径下最后的一个单词。比如，`math/rand`路径下的包文件头部就是`package rand`的声明。

### 公有方法名

在GO语言中，公有方法的命名规则是首字母大写。

当引入一个包的时候，你只能使用它的公有方法，任何非公有方法都不能在包外部使用。

### 函数

一个函数可以拥有多个参数。

```go
package main

import "fmt"

func add(x int, y int) int {
    return x + y
}

func main() {
    fmt.Println(add(1, 2))
}
```

注意到，参数类型是放在参数名之后的。同时，参数声明可以简写成`x, y int`。

一个函数可以返回多个数据：

```go
package main

import "fmt"

func swap(x, y string) (string, string) {
    return y, x
}

func main() {
    a, b := swap("hello", "world")
    fmt.println(a, b)
}
```

### 命名返回值

Go函数返回的参数可以被命名，名字在函数顶部声明，通过`return`返回。

```go
package main

import "fmt"

func split(sum int) (x, y int) {
    x = sum * 4 / 9
    y = sum - x
    return
}

func main() {
    fmt.Println(split(17))
}
```

### 变量

`var`声明可以同时在包作用域和函数作用域使用，而`:=`只能在函数内使用。

可以一次性初始化多个变量：`i, j int:= 1, 2`，同时初始化的变量如果类型相同，则类型放在最后一个函数之后。

同时初始化的变量类型不需要相同：`i, j, k = 1, false, "no"`。

### 变量类型

基础类型如下：

```
bool

string

int int8 int16 int32 int64
uint uint8 uint16 uint32 uint64 uintptr

byte // 相当于uint8

rune // 相当于int32，代表Unicode编码

float32 float64

complex64 complex128
```

`int`, `uint`和`uintptr`在32位系统和64位系统上长度不一样，除非有特别原因，否则在声明整数类型的时候最好用`int`类型。

```go
package main

import (
    "fmt"
    "math/cmplx"
)

var (
    ToBe bool = false
    MaxInt uint64 = 1<<64 - 1
    z complex128 = cmplx.Sqrt(-5 + 12i)
)

func main() {
    fmt.Printf("Type: %T Value: %v\n", ToBe, ToBe)
    fmt.Printf("Type: %T Value: %v\n", MaxInt, MaxInt)
    fmt.Printf("Type: %T Value: %v\n", z, z)
}
```

### 默认值

如果不显式赋予变量初始值，会用系统默认的值：

- 数字类型是`0`
- `bool`类型是`false`
- `string`类型是`""`

显式赋值的方法是T(v)，将v转换成T，例如`var x float = float(y)`。

如果不显式地赋予变量初始值，例如`i := 42`，变量类型根据等式右边的值而定：

```go
i := 32 // int
f := 1.1 // float64
g := 1.2 + 1.5i //complex128
```

### 常量

常量不能用`:=`赋值：`const Pi = 3.14`，命名上最好用首字母大写。