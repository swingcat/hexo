---
title: rust
date: 2019-05-20 15:10:59
tags: rust
---

Rust语言添加了一些区别于其他高级语言的新特性，特别是内存管理方面，使其成为一门非常有必要学习的语言。下面是本人参照[官方文档](https://doc.rust-lang.org/book/title-page.html)作的摘要。

# 说明

本文适用于Rust 1.41.0及以上版本，原文档可以通过命令`rustup docs --book`打开。

# 前言

Rust是对作为程序员的你手上已有权力的补充，无论你来自哪个语言或是哪个技术栈背景。

例如同计算机底层打交道的“系统层级”，不论是内存管理，数据表示或者是并行，rust都可以在不失效率的情况下同时保证安全性和效率。

Rust还可以开发高级应用比如CLI app，服务器等应用。

最终生成的是二进制文件，在相关平台上编译后可以直接执行。

# Hello world

```rust
fn main() {
    println!("hello world");
}
```

`main`函数是程序的入口；`!`表示`println`是一个**macro**；以`;`结尾。

程序的编译和执行是分开的。

# cargo

Cargo是Rust的build system和package manager。

```sh
cargo new hello_cargo
cd hello_cargo
```

项目初始化的同时，还创建了`.gitignore`文件

```sh
cargo build
```
此方法编译好的可执行程序路径是`target/debug`，同时在外层创建好`Cargo.lock`文件记录安装包的版本

```sh
cargo run
```

这个命令同时包含编译和执行

```sh
cargo check
```

这个命令只检查，不生成二进制

```sh
cargo build --release
```
适用于发布版本，使可执行程序更快，但需要更长时间编译。目标文件在`target/release`目录下。

# Guessing Game

```rust
use std::io;

fn main() {
    println!("Guess the number");
    
    println!("input your guess:");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");
    
    println!("you guessed: {}", guess);
}
```

在rust中，变量默认是`immutable`；`String`类型是标准库提供的growable, utf-8编码的文本类型；`::`表明`new`是**associated function**，也就是其他语言中的`static method`；`&`表示`guess`是**reference**，可以被其余的代码直接拿过来用而不用重新拷贝一份，reference默认也是`immutable`的，所以guess前面要加上**mut**。

`read_line`返回的是**Result**枚举类型，包括`Ok`和`Err`，标准库里有许多Result类型，目的是包含错误处理的信息。io::Result的instance有`expect`方法，如果instance是Err，就crash掉程序并且打印括号内的错误信息，如果是Ok则返回包含信息的数据。

### 添加随机数

编辑Cargo.toml文件：

```
[dependencies]

rand = "0.3.14"
```

```rust
use rand::Rng;
...
let secret_number = rand::thread_rng().gen_range(1, 101);
...
```

### 比较

```rust
use std::cmp::Ordering;
...
match guess.cmp(&secret_number) {
    Ordering::Less => println!("too small"),
    Ordering::Greater => println!("too big"),
    Ordering::Equal => println!("you win"),
}
...
```

`match`表达式由**arms**组成。`arm`就是match内每个逗号前面的内容，包括pattern和代码块。


此时程序报错，由于比较的类型不符，一个是std::string::String，另一个是integer，解决办法：

```rust
let guess: u32 = guess.trim().parse()
    .expect("please type a number");
```

rust允许**shadow**一个同名变量（此处是`guess`）；`trim`方法去掉了用户输入尾部的`\n`；`parse`方法把String转换成数字类型，此处用`:`注释为`u32`。

### 循环

```rust
loop {
    ...
}
```

### 跳出循环

```rust
Ordering::Equal => {
    break;
}
```

### 处理错误输入

```rust
let guess: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
}
```

### 最终代码

```rust
use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("guess the number");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {
        println!("input your guess");
        
        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("faild to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("you guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("too small"),
            Ordering::Greater => println!("too big"),
            Ordering::Equal => {
                println!("you win");
                break;
            }
        }
    }
}
```

# 通用编程概念

### Variables和Mutability

变量和reference默认是immutable，即：一旦赋值，就不能改变。

### Variables和Constants的区别

constants不能用`mut`关键字，并且总是immutable的；constants使用`const`关键字而不是`let`，并且**总是**要annotate数据类型；constatns可以在任何作用域内declare，例如顶作用域；constants只能通过constant expression赋值，而不能是运行时（因为赋值发生在编译期）。

```rust
const MAX_POINTS: u32 = 100_000;
```

### Shadowing

```rust
let x = 5;
let x = x + 1;
```

shadow和mut不同，要重复使用`let`关键字；通过使用shadow，可以在对变量进行一些改变之后重新设置为immutable；shadow还可以改变数据类型，mut不行。

### 数据类型

scalar and compound

Rust是静态类型的语言，也就是说，在编译的时候要知道所有数据的数据类型，所以能annotate type的地方尽量加上。

#### scalar types: integer, floating-point number, Boolean, character

integer types: i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, isize, usize；其中isize和usize取决于计算机。默认是i32，因为总体来说最快

例子：98_322, 0xff, 0o77, 0b1111_0000, b'A'

floating_point type: f32, f64，默认是f64，因为现代cpu上f32和f64运行速度差不多

char type: 4 bytes，包含中文等各种字符、表情

#### compund types：可以将多种数据纳入一种类型，rust有两种内置的类型（tuple和array）

tuple：长度固定，一旦定义则无法更改，内部数据类型无需统一

```rust
let tup: (i32, f64, u8) = (500, 4.3, 1);
```

destructuring:

```rust
let tup = (500, 3.3, 1);
let (x, y, z) = tup;
println!("the value of y is: {}", y);
```

直接访问：

```rust
let x: (i32, f64, u8) = (500, 1.1, 1);
let a = x.0;
let b = x.1;
let c = x.2;
```

array: 内部数据类型必须统一；固定长度；存在stack而不是heap中；如果不确定长度就用vector。

```rust
let a: [i32; 5] = [1, 2, 3, 4, 5];
let b = [3; 5];
```

### Function

```rust
fn main() {
    let x = 5;

    let y = {
        let x = 3;
        x + 1
    };

    println!("the value of y is: {}", y);
}
```

### Control Flow
```rust
fn main() {
    let condition = true;
    let number = if condition {
        5
    } else {
        "six"
    }; // 错误（编译器无法判断number类型）
    ...
}
```

```rust
fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };
    ...
}
```

```rust
fn main() {
    let mut counter = 3;

    while counter != 0 {
        counter -= 1;
    }
    ...
}
```

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }
}
```

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }
}
```

```rust
fn main() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
```

# Ownership

Ownership是rust最独特的feature，可以保证内存安全并无需垃圾回收。

一般语言的做法是在程序运行的过程中不断查找不再使用的变量，还有些语言需要程序员手动创建并回收。Rust用的是第三种：内存通过编译器在编译期验证特定规则，来保证内存得到合适的管理。所有ownership的特性都不会影响运行程序的效率。

### stack和heap

stack和heap都是程序运行时的内存空间；stack就像一叠盘子，last in first out，push and pop off；stack内的数据必须是**定长**的，任何不定长的数据都在heap中；heap存储数据的方式是，操作系统先找一块足够大的空间，标记为使用状态，返回一个指针，这个过程也被称为`allocating on the heap`。

push stack比allocate heap快，因为程序不需要寻址，同时allocate space on heap也需要更多的操作，因为操作系统得先找一段足够大的空间。

当程序调一个函数时，入参和局部变量都被push到stack中，当函数结束就被pop off stack。

### Ownership Rules

- 每个value都有且只有一个被称作`owner`的变量
- 当owner离开作用域，对应的value就被drop掉

### String Type

存在heap中，可以改变大小

```rust
let mut s = String::from("hello");
s.push_str(", world");
println!("{}", s);
```

当作用域结束，rust会自动为变量调drop函数，将内存返还给操作系统

```rust
let s1 = String::from("hello");
let s2 = s1;
```

一个String包含三个部分：指针（指向实际内容）, length, capacity; 这些数据存在stack中；length是实际使用的内存量（bytes），capacity是从操作系统获得的内存（bytes）。

当把s1 assign给s2时，String的三部分被复制，但是对应heap中的实际内容并没有复制，这是为了防止在实际内容过多时影响性能，在有的编程语言中这种只复制引用不复制实际内容的操作被成为`shallow copy`，但是区别于shallow copy，rust同时会`invalidate` s1，以此保证在drop的环节实际内容不会被drop两次，这种操作被成为**move**，所以这里的表达应该是，s1被`move`到s2中。

除此之外，rust默认不会`deep copy`数据，如果要deep copy数据就用`clone`方法：

```rust
let s1 = String::from("hello");
let s2 = s1.clone();
```

下面这段代码是例外：

```rust
let x = 5;
let y = x;
println!("x = {}, y = {}", x, y);
```

像integer这种数据有固定的长度，直接存在stack中，copy操作很快，所以rust默认这里不会invalidata x，而是直接clone。

integer有`Copy trait`，拥有此特性的数据类型在赋值的时候不会invalidate之前的变量；如果一个类型实现了`Drop trait`，则不能再实现`Copy trait`

拥有Copy trait的数据类型有这些：

- 整数类型
- Boolean类型
- 浮点数类型
- char类型
- 内部数据**只**拥有Copy trait的tuple

### Ownership and Function

传入function中的参数和返回值也遵循ownership的规则（`move`或者`copy`），函数执行完毕后，释放局部变量的顺序与声明变量的顺序相反；`reference`可以使用某些变量的同时without taking ownership；这种用`reference`作为函数参数的行为叫做`borrowing`,当然，borrow的reference默认是immutable的；如果要mutable，要加上`mut`关键字

但是mutable reference有个限制，**在同一个作用域内只能有一个**，下面的代码会报错：

```rust
let mut s = String::from("hello");
let r1 = &mut s;
let r2 = &mut s;
```

这么做的好处是，在编译器就可以发现可能出现data races的情况。`data races`是指同时出现如下几种情况：

- 两个或以上的指针同时操作同一个数据
- 至少有一个指针是写数据
- 没有使用同步数据的机制

不同作用域可以有同一个mutable变量的指针：

```rust
let mut s = String::from("hello");
{
    let r1 = &mut s;
}
let r2 = &mut s;
```

下面的会报错：

```rust
let mut s = String::from("hello");

let r1 = &s; // read only
let r2 = &s; // read only, ok
let r3 = &mut s; // read and write, ERROR
```

### Dangling references

dangling reference: 指向的数据已被清空的指针（清除指针需要在该指针指向的数据被释放之前）；下面代码会报错：

```rust
fn main() {
    let a = dangle();
}

fn dangle() -> &String {
    let s = String::from("hello");
    &s
}
```

解决办法是直接返回一个String

### Rules of References

- 同一时间，要么只有一个mutable reference，要么只有若干个immutable references（可以有一个写锁，或者若干个读锁）
- references must always be valid（不能指向空数据）

### Slice

还有一个不会take ownership的类型叫做`slice`，slice让你能访问集合里的`连续片段`而不是整个集合。

### String Slices

```rust
let s = String::from("hello world");
let hello = &s[0..5];
let world = &s[6..11];
```

这里变量hello和world只是指向部分s的指针，string表达式（"hello world"）就是string slice类型，默认immutable

```rust
fn first_world(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}
```

这里更合适的做法是`first_world(s: &str)`，这样更加通用

# Struct

struct是自定义的各种数据类型的组合

### 定义和struct初始化

struct就像tuple，相同点是都可以容纳不同的数据类型，不同点是，struct内的不同数据类型可以显式申明，所以struct更灵活，你不需要通过下标去获取内部的元素。

```rust
struct User {
    username: String, // field
    email: String,
    sign_in_count: u64,
    active: bool,
}

let mut lewis = User {
    email: String::from("lewis@aaa.com"),
    username: String::from("lewis"),
    active: true,
    sign_in_count: 1,
};

lewis.email = String::from("another@asdf.com");
```

struct初始化时所有field都是同样性质的mutability

```rust
fn build_user(username: String, email: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}
```

### struct初始化简写

```rust
let user2 = User {
    email: String::from("eeee@ss.com"),
    username: String::from("name"),
    ..lewis
};
```

user2剩余的属性通过lewis构造

### tuple struct

适用于想自己命名一个tuple以区别默认类型，但又不想为内部field取名的情况

```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);
```

### unit-like struct

没有field的struct，适用于实现不带内部数据的trait

### ownership of struct data

User struct定义里是String类型，这是有意为之，由于我们希望struct data拥有field的ownership，和相同的生命周期；如果用&str类型，需要指定对应的lifetime specifier

### 例子

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

fn main() {
    let rect1 = Rectangle{ width: 32, height: 29 };
    println!("rect1 is {:?}", rect1);
}
```

这里struct定义前面的`#[derive(Debug)] annotation`使println! macro里可以用{:?}, {:#?}这些符号打印struct内部的信息

### method语法

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size }
    } // Rectangle::square(1), associated function
}

fn main() {
    let rect = Rectangle{ width: 32, height: 32 };
    println!("the area is {}", rect.area());
}
```

### automatic referencing and dereferencing

当引用对象的方法，或者指针指向的对象的方法的时候，通通写成`object.method()`，rust会自动转化object为此对象

# Enums and Pattern Matching

```rust
enum IpAddrKind {
    V4(u8, u8, u8, u8),
    V6(String),
}

let four = IpAddrKind::V4(127, 0, 0, 1);
let six = IpAddrKind::V6(String::from("::1"));
```

```rust
enum Message {
    Quit, // no data associated
    Move { x: i32, y: i32 }, // anonymous struct
    Write(String),
    ChangeColor(i32, i32, i32),
}
```

上面的enum相当于：

```rust
struct  QuitMessage; // unit struct
struct MoveMessage {
    x: i32,
    y: i32,
}
struct WriteMessage(String); // tuple struct
struct ChangeColorMessage(i32, i32, i32); // tuple struct
```

但是如果这样定义的话，这四种message没办法归为一类

`enum`和`struct`还有一个相同点，就是都可以定义`method`

```rust
impl Message {
    fn call(&self) {
        //
    }
}

let m = Message::Write(String::from("hello"));
m.call();
```

### `Option` Enum相比较于Null的优势

`Option enum`使用得太频繁，以至于`Some`和`None`可以直接使用，而不需要带上前缀`Option::`

其中，`Some(T)`和`None`是`Option<T>`的variants

```rust
enum Option<T> {
    Some(T),
    None,
}

let some_number = Some(5);
let some_string = Some("some string");

let absent_number: Option<i32> = None; // annotation(Option<i32>) necessary!
```

如果使用的是`None`，要指定`Option<T>`中`T`的具体类型

用Option<T>而不是直接用Null的原因是，Option和T是不同类型，编译器不让两者直接做运算，这样就降低了因为出现null而发生错误的风险：

```rust
let x: i8 = 5;
let y: Option<i8> = Some(5);

let sum = x + y; // error
```

### match

```rust
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1, // arm
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("state quarter from {:?}", state);
            25
        },
    }
}
```

### match with Option<T>

```rust
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    } // 要判断Option所有可能出现的情况
}
```

### _ Placeholder

当不关心剩余情况时用`_`

```rust
let some_u8_value = 0u8;
match some_u8_value {
    1 => println!("one"),
    3 => println!("three"),
    _ => (),
}
```

### if let

如果只关心一种情况的话就用`if let`

```rust
let some_u8_value = Some(0u8);
match some_u8_value {
    Some(3) => println!("three"),
    _ => (),
}
```

等价于

```rust
if let Some(3) = some_u8_value {
    println!("three");
}
```

也可以和else混合用

```rust
let mut count = 0;
if let Coin::Quarter(state) = coin {
    println!("{:?}", state);
} else {
    count += 1;
}
```

# Packages, Crates, Modules

`rust module system`包括

- Packages: A Cargo feature that lets you build, test, and share crates
- Crates: A tree of modules that produces a library or executable
- Modules and use: Let you control the organization, scope, and privacy of paths
- Paths: A way of naming an item, such as a struct, function, or module

`cargo new my-project`创建了一个新的package，默认入口是`src/*.rs`

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();
}
```

```rust
fn serve_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();
    }

    fn cook_order() {}
}
```

`struct`的属性要声明`pub`

```rust
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }
}

pub fn eat_at_restaurant() {
    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");
}
```

而`enum`只需要在顶层声明即可

```rust
mod back_of_house {
    pub enum Appetizer {
        Soup,
        Salad,
    }
}

pub fn eat_at_restaurant() {
    let order1 = back_of_house::Appetizer::Soup;
    let order2 = back_of_house::Appetizer::Salad;
}
```

用`use`引入mod

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

use crate::front_of_house::hosting; // or `use self::front_of_house::hosting;`

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

用`as`重命名

```rust
use std::io::Result as IoResult;
```

用`pub use`重新暴露作用域

```rust
pub use crate::front_of_house::hosting;
```

包解构

```rust
use std::io::{self, Write};
```

通配符

```rust
use std::collections::*;
```

# Common Collections

非定长的数据存在heap中

- vector
- string
- hash map

### Vector

vector允许**同种**数据类型在内存中**连续**存储

新建

```rust
let v: Vec<i32> = Vec::new(); // annotation important!

let v = vec![1, 2, 3]; // macro
```

更新

```rust
let mut v: Vec<i32> = Vec::new();
v.push(2);
```

作用域结束，内部的`vector`也会被清除

读取内部元素的两种方法

```rust
let v = vec![1, 2, 3, 4, 5];

let third: &i32 = &v[2]; // 1

match v.get(2) {
    Some(num) => println!("{}", num),
    None => (),
} // 2
```

第一种是用`&`和`[]`读取元素的`reference`，第二种是用`get`方法获取`Option<&T>`；其中第一种如果下标越界会引起程序panic

vector的指针同时遵循borrow和ownership原则：

```rust
let mut v = vec![1, 2, 3, 4, 5];
let first = &v[0]; // immutable reference
v.push(6); // error
```

vector改变的时候，有可能因为空间不够而要重新开辟连续存储空间，第一个元素的指针可能失效

遍历vec内的数值：

```rust
let mut v = vec![1, 2, 3];
for i in &mut v {
    *i += 2; // dereference operator `*` is necessary
}
```

用`enum`来包裹不同类型的数据构成`vec`

```rust
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

let row = vec![
    SpreadsheetCell::Int(3),
    SpreadsheetCell::Text(String::from("blue")),
    SpreadsheetCell::Float(3.4),
];
```

### String

rust自带的string literal类型是string slice；String类型是growable，mutable，owned，utf-8编码的字符串类型；标准库中还有别的字符串类型：OsString, OsStr, CString, CStr

```rust
let mut s = String::new();
```

初始化用`to_string`函数，实现`Display trait`

```rust
let s = "initial contents".to_string();
```

也可以用`String::from`

```rust
let s = String::from("initial content");
```

用`push_str`和`push`来append string

```rust
let mut s = String::from("foo");
s.push_str("bar");
s.push('l');
```

也可以用`+`和`format!`来拼接

```rust
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // 注意此处s1已经被moved，不能再使用了
```

`+`的实现类似下面的

```rust
fn add(self, s: &str) -> String {
```

String类型只能+&str，而不能两个String类型相加，但是这里加的是&String类型又是为何？真是因为rust有`defer`强制转换，把`&s2`转换成`&s2[..]`；其次，add函数take ownership of self（self不带&），所以s1在此之后就不能使用

如果str太多，`format!`会比较合适：

```rust
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");

let s = format!("{}-{}-{}", s1, s2, s3);
```

`format!`不会take ownership

String包裹了一层`Vec<u8>`，不支持index，所以`s[0]`这种取法是会报错的

从rust角度看，utf-8实际上有三种表现形式：bytes, scalar values, grapheme clusters(letters);

比如“नमस्ते”，u8形态是这样：

```
[224, 164, 168, 224, 164, 174, 224, 164, 184, 224, 165, 141, 224, 164, 164,
224, 165, 135]
```

Unicode scalar values是这样：

```
['न', 'म', 'स', '्', 'त', 'े']
```

第四个和第六个没有任何意义，如果从grapheme clusters的角度看是这样：

```
["न", "म", "स्", "ते"]
```

String类型不能通过下标取值还有一个原因是，index操作一般消耗O(1)的时间，但是String类型要从头开始遍历，看有多少个合法的字符

虽然不允许下标取值，但是可以取string slice:

```rust
let hello = "Здравствуйте";

let s = &hello[0..4];
```

注意：如果取&hello[0..1]会报错，因为index 1不是一个合法字符，所以最好也避免string slice取值

推荐的做法是用`chars`，`bytes`方法：

```rust
for c in "नमस्ते".chars() {
    println!("{}", c);
}
```

会返回以下内容:

```
न
म
स
्
त
े
```

```rust
for b in "नमस्ते".bytes() {
    println!("{}", b);
}
```

返回：

```
224
164
// ...
165
135
```

grapheme clusters的获取没有包含在标准库里，需要去`crates.io`找

### Hash Map

创建：

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow", 50));
```

和vector一样，hash maps的数据也存储在heap里。所有的key和所有的value需要是同种类型。

还有一种创建方法是用`collect`方法：

```rust
use std::collections::HashMap;

let teams = vec![String::from("Blue"), String::from("Yellow")];
let initial_scores = vec![10, 50];

let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
```

此处`HashMap<_, _>` type annotation不能省略，下划线表示缺省，由data类型确定

```rust
use std::collections::HashMap;

let name = String::from("color");
let value = String::from("red");

let mut map = HashMap::new();
map.insert(name, value);
// name和value从此之后就不能用了
```

查找：

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

let name = String::from("Blue");
let score = scores.get(&name);
```

这里score返回的是`Some(&10)`，如果HashMap中没有查到对应值则返回`None`

```rust
use std::collection::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

for (k, v) in &scores {
    println!("{}: {}", k, v);
}
```

修改：

overwrite:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Blue"), 20);

println!("{:?}", scores);
```

insert if not exist:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();
scores.insert(String::from("Blue"), 10);

scores.entry(String::from("Yellow")).or_insert(50);
scores.entry(String::from("Blue")).or_insert(50);

println!("{:?}", scores);
```

最后打印`{"Yellow": 50, "Blue": 10}`

updata based on old value:

```rust
use std::collections::HashMap;

let text = "hellow world wonderful world";

let mut map = HashMap::new();

for word in text.split_whitespace() {
    let count = map.entry(word).or_insert(0);
    *count += 1;
}

println!("{:?}", map);
```

`or_insert`返回的是`&mut v`

`HashMap`默认采用一种较安全的hash算法，如果想用更快的算法，可以自己实现`BuildHasher trait`，也可以从`crates.io`寻找

# Error Handling

rust将错误归为两类：recoverable和unrecoverable；对于recoverable error，比如没有找到文件，可以通知用户重新操作；unrecoverable指的是bug，比如数组越界

一般语言没有区分这两类，而是一视同仁让用户处理比如exception；rust用`Result<T, E>`表示recoverable errors，用`panic!`来停止程序运行

### Unrecoverable Errors with panic!

一般来说，当panic发生时程序开始`unwinding`，rust开始回溯stack并开始清除内存，但是这样比较大费周章。还有一种办法是`abort`，程序结束但不回收内存，内存由操作系统回收。如果你想要编译后的二进制文件尽量小，你可以在Cargo.toml文件的`[profile]`部分加上`panic = 'abort'`，比如：

```
[profile.release]
panic = 'abort'
```

### Recoverable Errors with Result

rust用一个enum表示可以恢复的错误：

```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

以下代码用来处理错误信息：

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => {
            panic!("Problem opening file: {:?}", error)
        },
    };
}
```

还可以判断错误类型：

```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating file: {:?}", e),
            },
            other_error => panic!("Problem opening file: {:?}", other_error),
        },
    };
}
```

还有一种更好的写法：

```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating file: {:?}", error);
            })
        } else {
            panic!("Problem opening file: {:?}", error);
        }
    });
}
```

Panic on Error的两种简写方法：`unwrap`和`expect`

```rust
let f = File::open("hello.txt").unwrap();
let f = File::open("hello.txt").expect("Failed to open hello.txt");
```

Propagating Errors

```rust
use std::io;
use std::io::Read;
use std::fs::file;

fn read_username_from_file() -> Result<String, io::Error>{
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

下面是更加简洁的写法，功能和上面的函数完全相同：

```rust
use std::io;
use std::io::Read;
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error>{
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
```

然而 `match` 和 `?` 还是有区别的，每个通过`?`返回的错误会经过`from`函数的处理，这个函数定义在`From trait`中，用来转换不同类型的error，以达到返回错误类型的一致。所以只要error类型定义了`from`方法，剩下的就都交给`?`来解决

上面的函数可以继续简化：

```rust
use std::io;
use std::io::Read;
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("hello.txt")?.read_to_string(&mut s)?;

    Ok(s)
}
```

最简化的写法是rust提供的内置函数：

```rust
use std::io;
use std::fs;

fn read_username_from_file() -> Result<String, io::Error> {
    fs::read_to_string("hello.txt")
}
```

`?` 只能用在返回`Result<T, E>`的函数后面

### To panic! or Not to panic!

不同的场景需要采取不同的错误机制，例如：Examples, Prototype Code, and Tests，可以随意设置；Cases in Which You Have More Information Than the Compiler可以直接panic

可以创建自定义的类型来验证数据：

```rust
pub struct Guess{
    value: i32,
}

impl Guess{
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("asdf");
        }

        Guess {
            value
        }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}
```

# Generic Types, Traits, and Lifetimes

### Generic Data Types

```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.1, y: 1.2 };
}
```

```rust
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

fn main() {
    let p = Point { x: 5, y: 19 };

    println!("p.x = {}", p.x());
}
```

```rust
struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

fn main() {
    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "Hello", y: 'c'};

    let p3 = p1.mixup(p2);

    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}
```

性能：编译的时候编译器会将`generics`替换成对应的concrete types，所以使用generics不会有任何性能损耗

### Traits: Defining Shared Behavior

```rust
pub trait Summary {
    fn summarize(&self) -> String;
}
```

```rust
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}
```

实现trait有一点要注意，就是不能为包外的struct实现包外的trait

还可以添加默认实现：

```rust
pub trait Summary {
    fn summarize(&self) -> String {
        String::from("read more")
    }
}
```

trait中的方法可以调同一个trait的另一个方法：

```rust
pub trait Summary {
    fn summarize_author(&self) -> String;

    fn summarize(&self) -> String {
        format!("read more from {}", self.summarize_author())
    }
}
```

可以将traits当作参数：

```rust
pub fn notify(item: impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```

impl只是语法糖，真正的写法是这样：

```rust
pub fn notify<T: Summary>(item: T) {
    // snip
}
```

比较一下这三种写法：

```rust
pub fn notify(item1: impl Summary, item2: impl Summary) {}
pub fn notify<T: Summary>(item1: T, item2: T) {}
pub fn notify<T: Summary, U: Summary>(item1: T, item2: U) {}
```

第一种写法的两个参数可以是不同类型，第二种限制两个参数必须是同种类型，第三种和第一种等价

还可以实现多种trait：

```rust
pub fn notify(item: impl Summary + Display) {}
pub fn notify<T: Summary + Display>(item: T) {}
```

为了可读性，如果要实现的trait太多，可以采用这种写法：

```rust
fn some_function<T, U>(t: T, u: U) -> i32
    where T: Display + Clone,
        U: Clone + Debug
{}
```

返回值也可以用trait：

```rust
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("of course, as you probably already know, people"),
        reply: false,
        retweet: false,
    }
}
```

**注意**：返回trait的值只能是同一种类型（编译环节要知道变量的具体类型），以下代码会报错：

```rust
fn returns_summarizable(switch: bool) -> impl Summary {
    if switch {
        NewsArticle {
            // snip...
        }
    } else {
        Tweet {
            // snip...
        }
    }
}
```

`generics` 和 `trait` 的结合：

```rust
fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() { // need Copy trait
        if item > largest { // need PartialOrd trait
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![1, 2, 3, 4];

    let result = largest(&number_list);
    println!("the largest number is {}", result);

    let char_list = vec!['a', 'b', 'c'];

    let result = largest(&char_list);
    println!("the largest char is {}", result);
}
```

trait bounds也可以用来有条件地实现不同方法：

```rust
use std::fmt.Display;

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self {
            x,
            y,
        }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("the larger is x = {}", self.x);
        } else {
            println!("the larger is y = {}", self.y);
        }
    }
}
```

也可以为已实现某种特定trait的数据类型实现trait，这种实现方式称为`blanket implementation`，rust标准库中有许多这种实现方式：

```rust
impl<T: Display> ToString for T {
    // snip...
}
```

接着所有实现Display trait的类型都可以调用ToString trait里的to_string方法：

```rust
let s = 3.to_string();
```

在动态语言里如果调用了未实现该方法的对象方法会报错，但rust把这一步搬到了编译环节

## Validating References with Lifetimes

**Rust中的每一个`reference`都有`lifetime`**。大多数时候lifetime都是隐式的，可以推断的，但是当歧义产生的时候我们就需要annotate lifetime，就像变量类型一样。

lifetime主要的作用是避免dangling references：

```rust
{
    let r;

    {
        let x = 5;
        r = &x;
    }

    println!("r: {}", r);
}
```

此处x作用域结束后已经销毁，但指向x的指针在外层作用域仍然存在，这在rust中是不允许的，并且通过`borrow checker`机制来保证（值的生命周期一定要比引用的生命周期长）

```rust
fn main() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);
}
```

如果longest函数这样实现会报错：`missing lifetime specifier`，没有指定返回值的生命周期

```rust
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

事实上我们也不知道返回值的生命周期应该是参照x还是y，因为不同的长度会返回不同的值！解决办法是`lifetime annotation`

`lifetime annotation`不会改变reference的生命周期，就像generic type一样，不会改变实际的数据类型，只是用来描述生命周期，写法如下：

```rust
&i32        // a reference
&'a i32     // a reference with an explicit lifetime
&'a mut i32 // a mutable reference with an explicit lifetime
```

单独的'a没有什么意义，要和其他变量组合用：

```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

这里'a表示传入的参数x和y以及返回值**至少**能活'a这么长，或者说，返回值的生命周期等于x、y生命周期的较小值；这里并不是说要改变参数的生命周期，而是让borrow checker来验证此规则

这样做的好处是，编译器可以在编译阶段检查出函数返回的指针可能指向空数据的情况，从而迫使你只能将该函数用在局部作用域内，从而避免空指针

```rust
fn main() {
    let string1 = String::from("long string is long");
    let result;
    {
        let string2 = String::from("xyz");
        result = longest(string1.as_str(), string2.as_str());
    }
    println!("The longest string is {}", result);)
}
```

这段代码会报错：`error: string2 does not live long enough`

但是，如果是以下实现就不会报错：

```rust
fn longest<'a>(x: &'a str, y: &str) -> &'a str {
    x
}
```

因为此处的返回值只和x有关；如果返回值和入参无关的话，最好返回一个owned data type而不是一个reference；lifetime的本质就是将不同的参数和返回值联系到一起，确保不会出现dangling references

struct内部属性也可以包含指针：

```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn main() {
    let novel = String::from("call me ishmael. some years ago...");
    let first_sentence = novel.split('.')
        .next()
        .expect("Could not find a '.'");
    let i = ImportantExcerpt { part: first_sentence };
}
```

这里的`'a`表示`ImportantExcerpt`的生命周期不能比其中的`part`属性长；这里`i`变量实例的创建在`novel`之后，其part属性指向novel的一段str

lifetime annotation在某些情况下可以省略，编译器会按照下面三种情况来推断：

1. 函数和方法的所有入参如果是引用的话都会带一个lifetime annotation，一个参数的话就是`fn foo<'a>(x: &'a i32)`，两个参数的话就是`fn foo<'a, 'b>(x: &'a i32, y: &'b i32)`

2. 如果只有一个入参，则所有的出参都会带上该入参的生命周期：`fn foo<'a>(x: &'a i32) -> &'a i32`

3. 如果有多个入参生命周期，其中的一个是`&self`或者`&mut self`，则所有出参都会带上`self`的生命周期，例如：

```rust
impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention: {}", announcement);
        self.part
    }
}
```

有一个特殊的就是`'static`，表示整个程序的生命周期。所有的string literal都有`'static` lifetime：

```rust
let s: &'static str = "I have a static lifetime";
```

**在决定让变量`'static`之前，先好好想想该变量是否真的贯穿整个程序始终；比如报错信息，是动态生成还是一开始就存在内存中**

下面是结合泛型、trait和lifetime的函数：

```rust
use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

# 自动化测试

测试三部曲：

1. 建立data和state
2. 运行程序
3. 验证结果

`#[test]`标识测试代码

`cargo test # 生成二进制runner并执行test标识的函数，展示结果`

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(larger.can_hold(&smaller));
    }
}
```

`#[should_panic(expected = "Guess value must be less than or equal to 100")]`

Using Result<T, E> in Tests

```rust
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}
```

`#[cfg(test)]`限定编译器只有运行`cargo test`时才编译测试代码

单元测试可以测试private的代码

```rust
pub fn add_two(a: i32) -> i32 {
    internal_adder(a, 2)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn internal() {
        assert_eq!(4, internal_adder(2, 2));
    }
}
```

集成测试首先要在项目root创建`tests`文件夹

```rust
use adder;

#[test]
fn it_adds_two() {
    assert_eq!(4, adder::add_two(2));
}
```

# An I/O Project: Building a Command Line Program

读取环境变量

```rust
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
}
```

如果接收的变量有特殊字符（中文等）要用`std::env::args_os`，返回的是`OsString`

这里collect返回的Vec需要annotate类型

```rust
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = parse_config(&args);

    println!("query: {}", config.query);
    println!("filename: {}", config.filename);

    let contents = fs::read_to_string(config.filename)
        .expect("Something wrong reading the file");

    println!("with text:\n{}", contents);
}

struct Config {
    query: String,
    filename: String,
}

fn parse_config(args: &[String]) -> Config {
    let query = args[1].clone();
    let filename = args[2].clone();

    Config { query, filename }
}
```

这里用`clone`拷贝`String`可以不用管理`&str`指针的生命周期，虽然会带来一点性能损耗，但在这里是值得的

模块化代码：

```rust
// main.rs
use std::env;
use std::process;

use minigrep::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    println!("query: {}", config.query);
    println!("filename: {}", config.filename);

    if let Err(e) = minigrep::run(config) {
        println!("Application error: {}", e);

        process::exit(1);
    }
}
```

```rust
// lib.rs
use std::error::Error;
use std::fs;

pub struct Config {
    pub query: String,
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("not enough arguments");
        }
        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    println!("with text:\n{}", contents);

    Ok(())
}
```

## 测试驱动

1. 构建测试用例
2. 写一个简单的demo来通过测试用例
3. 改进demo的同时保证一直通过测试用例
4. 从步骤1重新开始

```rust
use std::error::Error;
use std::fs;
use std::env;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("not enough arguments");
        }
        let query = args[1].clone();
        let filename = args[2].clone();

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }

    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }

    results
}

pub fn search_case_insensitive<'a>(
    query: &str,
    contents: &'a str,
) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }

    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
```

终端的错误一般要送到标准错误流中，这样就可以在把标准输出流写入文件的同时可以看到终端上的错误日志；方法是使用`eprintln!`

# Functional Language Features: Iterators and Closures

- Closure: 类函数的数据结构，可以赋值给变量
- Iterator: 处理元素集合的一种方式

## Closures闭包: Anonymous Functions that Can Capture Their Environment

闭包是匿名函数，你可以存储在一个变量中，或者作为参数传递到其它函数中；不同于函数，闭包还可以获取定义作用域内的变量

闭包函数不需要声明入参和出参的数据类型；普通函数需要，是为了让使用者达成一致，而闭包函数是用在局部作用域内的，甚至不需要取名字

```rust
fn  add_one_v1   (x: u32) -> u32 { x + 1 }
let add_one_v2 = |x: u32| -> u32 { x + 1 };
let add_one_v3 = |x|             { x + 1 };
let add_one_v4 = |x|               x + 1  ;
```

其中`add_one_v3`和`add_one_v4`需要在定义域内被使用，这样编译器才能推断出参数的数据类型

但是被使用的同时，参数的数据类型需要保持统一，以下操作会报错`mismatched types`：

```rust
let closure = |x| x;

let s = closure(String::from("hello"));
let n = closure(5);
```

标准库提供`Fn FnMut FnOnce trait`，所有的闭包至少实现其中的一种

```rust
struct Cacher<T>
where
    T: Fn(u32) -> u32,
{
    calculation: T,
    value: Option<u32>,
}

impl<T> Cacher<T>
where
    T: Fn(u32) -> u32,
{
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation,
            value: None,
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            }
        }
    }
}
```

这里的Cacher有两个问题，一个是存储的value只能有一个值，可以变为`HashMap`解决；第二个是calculation闭包方法只能传入u32并且返回该值，可以通过引入`generic type`解决

```rust
fn main() {
    let x = 4;

    fn equal_to_x(z: i32) -> bool {
        z == x
    }

    let y = 4;

    assert!(equal_to_x(y));
}
```

每次闭包获取环境变量的时候，闭包内部会额外消耗内存来存储

`closure`有三种方法获取环境变量：

- `FnOnce`直接获取环境变量的`Ownership`，其中`Once`表明只能获取一次
- `FnMut`可以编辑环境变量
- `Fn` borrows values from the environment immutably

当创建`closure`的时候，rust会根据closure如何处理环境变量来自动推断使用那种`trait`；所有的闭包都会实现`FnOnce`，不`move`环境变量的也会实现`FnMut`，其次不需要`mutable access`的还会实现`Fn`

如果要强制`closure`获取环境变量的所有权，可以在参数列表前面加上`move`关键字；这种技巧的使用场景一般是开启新`thread`的时候

下面程序会报错：

```rust
fn main() {
    let x = vec![1, 2, 3];

    let equal_to_x = move |z| z == x;

    println!("can't use x here: {:?}", x);

    let y = vec![1, 2, 3];
    assert!(equal_to_x(y));
}
```

此处在声明`equal_to_x`的时候已经获取了x的`ownership`，所以下面的`println!`会报错

## Processing a Series of Items with Iterators

迭代器的作用是处理集合的迭代逻辑，同时决定何时结束

rust中的iterator是`lazy`加载的，也就说除非你使用迭代方法，否则不会执行，下面的代码不会产生任何实际效果：

```rust
let v1 = vec![1, 2, 3];
let v1_iter = v1.iter();
```

下面的代码将迭代器声明和执行分开

```rust
let v1 = vec![1, 2, 3];

let v1_iter = v1.iter();

for val in v1_iter {
    println!("Got: {}", val);
}
```

`Iterator`的定义类似下面这样：

```rust
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;

    // snip
}
```

注意：声明`Iterator`的时候要加上`mut`，因为`next`方法会改变迭代器内部的状态；在`for`循环里不需要声明`mutable`是因为循环内部获取了`ownership`并且在底层做了处理

`iter().next`方法返回的是`immutable reference`；如果需要`takes ownership`并且返回`owned values`，用`into_iter`；如果需要获取`mutable references`，用`iter_mut`

有些内部使用`next`的方法叫做`consuming adaptors`，这些方法会消耗掉整个`Iterator`，例如sum：

```rust
#[test]
fn iterator_sum() {
    let v1 = vec![1, 2, 3];
    
    let v1_iter = v1.iter();

    let total: i32 = v1_iter.sum();

    assert_eq!(total, 6);
}
```

还有一些方法可以将`Iterator`变成不同种类的`Iterator`，这些方法叫做`iterator adaptors`，例如`map`：

```rust
let v1: Vec<i32> = vec![1, 2, 3];

let v2: Vec<_> = v1.iter().map(|x| x + 1).collect();

assert_eq!(v2, vec![2, 3, 4]);
```

自定义`Iterator`

```rust
struct Counter {
    count: u32,
}

impl Counter {
    fn new() -> Counter {
        Counter { count: 0 }
    }
}

impl Iterator for Counter {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count < 5 {
            self.count += 1;
            Some(self.count)
        } else {
            None
        }
    }
}

fn main() {

}
```

还有复合使用的骚操作：

```rust
#[test]
fn saoMethod() {
    let sum: u32 = Counter::new()
        .zip(Counter::new().skip(1))
        .map(|(a, b)| a * b)
        .filter(|x| x % 3 == 0)
        .sum();
    assert_eq!(18, sum);
}
```

注意`zip`方法只能产生4对；第5对`(5, None)`永远没法产生（zip遇到集合中任一元素为None的直接返回None）

优化I/O项目：

去掉`clone`环节：

```rust
fn main() {
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    }); // env::args()直接传入，避免collect()环节

    // --snip--
}

impl Config {
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> { // 要遍历args, 所以需要加mut
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name"),
        };

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
```

改进search方法可读性：

```rust
pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}
```

性能方面，用`Iterator`比low level的`for`循环更快，因为rust在内部处理时把循环转换成了重复代码，所有的系数存在了寄存器中，快到飞起；虽然用起来是high level，但`Iterator`并不会带来运行时的性能损耗

# More About Cargo and Crates.io

`release profiles`是预定的可定制化的配置，在编译环节使用

Cargo.toml

```
[profile.dev]
opt-level = 0

[profile.release]
opt-level = 3
```

写有用的注释

```rust
/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}
```

创建一个crates.io账号，本地注册`~/.cargo/credentials`

```sh
$ cargo login [apikey]
```

发布程序

```sh
$ cargo publish
```

## Cargo Workspaces

本地包：

Cargo.toml

```
[workspace]

members = [
    "adder",
    "add-one",
]
```

```sh
cargo new add-one --lib
```

```
[dependencies]

add-one = { path = "../add-one" }
```

```sh
$ cargo run -p adder
```

外部包：

Cargo.toml

```
[dependencies]
rand = "0.5.5"
```

如果不同包都引用rand，但是rand属于不同的版本，rust会解析并统一，使之使用同一个版本的外部包

测试也可以分包进行:

```sh
cargo test -p add-one
```

安装二进制：

```sh
$ cargo install ripgrep
```

查看可用命令：

```
cargo --list
```

# Smart Pointers

`pointer`指的是一个在内存里拥有一个对应地址的变量，这个变量除了指向数据之外没有其他作用，也没有额外的开销

`smart pointer`不仅拥有普通指针的功能，还有额外的元数据和功能，同时是一种数据结构。`String`和`Vec<T>`也是智能指针

在rust中这两者的主要区别是，普通的引用只是`borrow data`，而在很多场景下，智能指针还`own the data they point to`

`smart pointer`通常由`struct`构建，同时实现`Deref`和`Drop` trait

内置常用的智能指针有以下几种：

- Box<T>在heap上分配内存
- Rc<T>，`reference counting`类型，enables multiple ownership
- Ref<T>和RefMut<T>，通过RefCell<T>获取，在runtime检查borrowing rules，而不是compile time

Box<T>的使用场景：

- When you have a type whose size can’t be known at compile time and you want to use a value of that type in a context that requires an exact size
- When you have a large amount of data and you want to transfer ownership but ensure the data won’t be copied when you do so
- When you want to own a value and you care only that it’s a type that implements a particular trait rather than being of a specific type

在编译器无法确定实际大小的数据类型叫`recursive type`，这种类型的内部有同种类型的数据。由于这种递归可能会重复无限次，所以rust没办法知道到底需要多大内存空间。

Lisp语言有个`cons list`的概念，定义类似下面：

```rust
enum List {
    Cons(i32, List),
    Nil,
}
```

如果要构建一个1,2,3的列表需要如下操作：

```rust
use crate::List::{Cons, Nil};

fn main() {
    let list = Cons(1, Cons(2, Cons(3, Nil)));
}
```

以上代码会报错：`recursive type has infinite size`

rust判断enum长度的时候，取的是最长的variant的长度作为参考，如果是recursive的话，便没有办法判断长度，改进方法如下：

```rust
enum List {
    Cons(i32, Box<List>),
    Nil,
}
```

```rust
fn main() {
    let x = 5;
    let y = &x;

    assert_eq!(5, x);
    assert_eq!(5, *y); // dereference
}
```

Box也可以这么用

```rust
fn main() {
    let x = 5;
    let y = Box::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y);
}
```

自定义smart pointer：

```rust
use std::ops::Deref;

struct MyBox<T>(T); // 只含一个元素的 tuple struct

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}
```

在后台，rust是这样处理`*y`的：

```rust
*(y.deref())
```

这里不直接返回self.0，而是返回指针，也是由于ownership，如果把内部数据移除self会出问题

`deref coercion`: 自动将实现`Deref trait`的数据转换成另一种数据类型的引用

```rust
fn hello(name: &str) {
    println!("Hello, {}", name);
}

fn main() {
    let m = MyBox::new(String::from("Rust"));
    hello(&m);
}
```

这里由于MyBox实现了Deref trait，自动被转换成了&String，然后String也实现了Deref trait，又一次调用deref转换成&str，所以hello方法可以直接调用；如果没有`deref coercion`，写法就会是下面这样子：

```rust
fn main() {
    let m = MyBox::new(String::from("Rust"));
    hello(&(*m)[..]);
}
```

当一个类型实现了`Deref trait`，rust会一直调用`Deref::deref`方法直到达成函数或方法的入参和定义参数类型匹配，并且这个过程是在编译期完成的，丝毫不影响运行时的性能！

和`Deref trait`一样，也有`DerefMut trait`来实现mutable reference

有三种情况rust会自动做`deref coercion`:

- From &T to &U when T: Deref<Target=U>
- From &mut T to &mut U when T: DerefMut<Target=U>
- From &mut T to &U when T: Deref<Target=U>

前两种好理解，第三种（由可写变成只读）是因为同一时间一个变量只能有一个可写的引用

## Drop trait

```rust
struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

fn main() {
    let c = CustomSmartPointer {
        data: String::from("my stuff"),
    };
    let d = CustomSmartPointer {
        data: String::from("other stuff"),
    };
    println!("CustomSmartPointers created.");
}
```

`Drop trait`在prelude里，所以不用显式引入；这里drop的顺序是d -> c

需要手动释放内存的场景很少，其中的一个是释放锁；rust不允许在代码里手动执行drop方法（避免重复执行），想手动释放只能用`std::mem::drop`，这个方法在prelude里，不需要显式引入

```rust
fn main() {
    let c = CustomSmartPointer {
        data: String::from("some data"),
    };
    println!("CustomSmartPointer created.");
    drop(c);
    println!("CustomSmartPointer dropped before the end of main.");
}
```

由于ownership的约束，变量的drop方法只会被调用一次

## Rc<T>

当我们要在heap上开一片内存存某个数据，来让程序里不同部分读取，但是又没法在编译期确定哪一段最后退出的时候就用Rc<T>；如果知道哪一段最后退出，将ownership赋给那个值就可以了

注意Rc<T>只适用于单线程的场景

设想一个场景，有三个列表：a: 5->10->Nil b: 3->a  c: 4->a

这里a列表同时被三个列表使用，如果是下面这种实现方式会报错：

```rust
enum List {
    Cons(i32, Box<List>),
    Nil,
}

use crate::List::{Cons, Nil};

fn main() {
    let a = Cons(5, Box::new(Cons(10, Box::new(Nil))));
    let b = Cons(3, Box::new(a)); // 这里a的ownership已经转交给了b
    let c = Cons(4, Box::new(a));
}
```

正确姿势：

```rust
enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::rc::Rc;

fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    let b = Cons(3, Rc::clone(&a));
    let c = Cons(4, Rc::clone(&a));
}
```

这里的`Rc::clone`并不会做`deep copy`，只会增加计数器；也可以用`a.clone()`，但是那样就无法同其他的深拷贝写法做区分

```rust
fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    println!("count after creating a = {}", Rc::strong_count(&a));
    let b = Cons(3, Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    {
        let c = Cons(4, Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
    }
    println!("count after c goes out of scope = {}", Rc::strong_count(&a));
    // the result is 1, 2, 3, 2
}
```

Drop trait会自动将reference count减1

## RefCell<T> and the Interior Mutability Pattern

RefCell<T>和Box<T>不同之处在于，前者是在**运行时**保证`borrowing rules`而后者是在编译期；后者报错是编译报错，前者报错是直接panic然后退出

当你认为程序不会违反borrowing rules，而编译器又无法保证这点时，RefCell就很有用；同Rc<T>一样，RefCell<T>也只适用于单线程

三种智能指针的使用场景：

- Rc<T> enables multiple owners of the same data; Box<T> and RefCell<T> have single owners.
- Box<T> allows immutable or mutable borrows checked at compile time; Rc<T> allows only immutable borrows checked at compile time; RefCell<T> allows immutable or mutable borrows checked at runtime.
- Because RefCell<T> allows mutable borrows checked at runtime, you can mutate the value inside the RefCell<T> even when the RefCell<T> is immutable.

A Use Case for Interior Mutability: Mock Objects

```rust
pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    pub fn new(messenger: &T, max: usize) -> LimitTracker<T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: out bounds");
        } else if percentage_of_max >= 0.9 {
            self.messenger.send("Warning: close to limit");
        } else if percentage_of_max >= 0.75 {
            self.messenger.send("Warning: over 75%");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct MockMessenger {
        sent_messages: Vec<String>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger {
                sent_messages: vec![],
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.push(String::from(message)); // Error: self是immutable的
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.len(), 1);
    }
}
```

上面的测试代码没办法编译通过，改进方法：

```rust
#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        // --snip--

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}
```

注意上面的`borrow_mut()`和`borrow()`，返回的数据类型分别是`RefMut<T>`和`Ref<T>`，两种类型都实现了`Deref`

`RefCell<T>`会追踪激活的`Ref<T>`和`RefMut<T>`的数量，就像`Rc<T>`一样，同时遵循borrow rules，在一个时间点同时有多个可读或者只有一个可写

以下代码会报错：

```rust
impl Messenger for MockMessenger {
    fn send(&self, message: &str) {
        let mut one_borrow = self.sent_messages.borrow_mut();
        let mut two_borrow = self.sent_messages.borrow_mut();

        one_borrow.push(String::from(message));
        two_borrow.push(String::from(message));
    }
}
```

Having Multiple Owners of Mutable Data by Combining Rc<T> and RefCell<T>

```rust
#[derive(Debug)]
enum List {
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::Rc;

fn main() {
    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Nil)));

    let b = Cons(Rc::new(RefCell::new(6)), Rc::clone(&a));
    let c = Cons(Rc::new(RefCell::new(10)), Rc::clone(&a));

    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
}
```

## Reference Cycles Can Leak Memory

如果代码里有循环引用可能会造成内存泄漏，因为变量的引用数永远无法清零

```rust
use crate::List::{Cons, Nil};
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
enum List {
    Cons(i32, RefCell<Rc<List>>),
    Nil,
}

impl List {
    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
        match self {
            Cons(_, item) => Some(item),
            Nil => None,
        }
    }
}

fn main() {
    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));

    println!("a initial rc count = {}", Rc::strong_count(&a)); // 1
    println!("a next item = {:?}", a.tail());

    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));

    println!("a rc count after b creation = {}", Rc::strong_count(&a)); // 2
    println!("b initial rc count = {}", Rc::strong_count(&b)); // 1
    println!("b next item = {:?}", b.tail());

    if let Some(link) = a.tail() {
        *link.borrow_mut() = Rc::clone(&b); // point a to b
    }

    println!("b rc count after changing a = {}", Rc::strong_count(&b)); // 2
    println!("a rc count after changing a = {}", Rc::strong_count(&a)); // 2

    // Uncomment the next line to see that we have a cycle;
    // it will overflow the stack
    // println!("a next item = {:?}", a.tail());
}
```

程序结束时，会先试图drop b，但是b仍被a指向，所以heap中的数据并不会被清除

Preventing Reference Cycles: Turning an Rc<T> into a Weak<T>

Rc和Weak通过`upgrade`和`downgrade`转换

```rust
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
struct Node {
    value: i32,
    parent: RefCell<Weak<Node>>, // child do not own parent!
    children: RefCell<Vec<Rc<Node>>>,
}

fn main() {
    let leaf = Rc::new(Node {
        value: 3,
        parent: RefCell::new(Weak::new()),
        children: RefCell::new(vec![]),
    });

    println!(
        "leaf strong = {}, weak = {}",
        Rc::strong_count(&leaf),
        Rc::weak_count(&leaf),
    );

    {
        let branch = Rc::new(Node {
            value: 5,
            parent: RefCell::new(Weak::new()),
            children: RefCell::new(vec![Rc::clone(&leaf)]),
        });

        *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

        println!(
            "branch strong = {}, weak = {}",
            Rc::strong_count(&branch),
            Rc::weak_count(&branch),
        );

        println!(
            "leaf strong = {}, weak = {}",
            Rc::strong_count(&leaf),
            Rc::weak_count(&leaf),
        );
    }

    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());
    println!(
        "leaf strong = {}, weak = {}",
        Rc::strong_count(&leaf),
        Rc::weak_count(&leaf),
    );
}
```

weak count不影响内存回收，当strong count为0的时候内存即被释放，这样就避免了循环引用造成的内存泄漏

# Fearless Concurrency

## 多线程

多线程会带来性能上的提升，但同时也会带来一些问题：

- Race conditions, where threads are accessing data or resources in an inconsistent order
- Deadlocks, where two threads are waiting for each other to finish using a resource the other thread has, preventing both threads from continuing
- Bugs that happen only in certain situations and are hard to reproduce and fix reliably

语言层面上实现多线程有多种方法，一种是调操作系统的1:1创建线程方法，也就是一个操作系统线程对应一个语言线程；还有一种是M:N，也就是M个语言层面的线程对应N个操作系统线程

非汇编程序在运行时会需要一些二进制代码的支持，这些运行时的代码体积或大或小；为了更小的运行时代码，rust选择只实现1:1创建线程的方法；如果需要M:N的方案，也有第三方crate提供支持

```rust
use std::thread;
use std::time::Duration;

fn main() {
    thread::spawn(|| {
        for i in 1..10 {
            prntln!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }
}
```

由于主线程结束的时候所有分线程也会被强制结束，以下方法可以强制等待分线程结束

```rust
fn main() {
    let handle = thread::spawn(|| {
        // snip
    })

    for i in 1..5 {
        // snip
    }

    handle.join().unwrap();
}
```

如果多线程执行的闭包函数中需要用到外层的变量，需要加`move`关键字，否则如果外层`drop`掉该变量的时候会引发错误

```rust
use std::thread;

fn main() {
    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        println!("here is a vector: {:?}", v);
    });

    handle.join().unwrap();
}
```

## Using Message Passing to Transfer Data Between Threads

类似golang中channel的概念：`Do not communicate by sharing memory; instead, share memory by communicating`

mpsc: multiple producer, single consumer

channel: transmitter + receiver

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = String::from("hi");
        tx.send(val).unwrap();
    });

    let received = rx.recv().unwrap(); // block main thread
    println!("Got: {}", received);
}
```

还有一个方法是`try_recv`，不会阻塞主线程，使用场景一般是在一个循环里每隔一段时间去获取一次

`send`方法会转移变量的`ownership`，以下代码会报错：

```rust
use std::sync::mpsc;
use std::thread;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let val = String::from("hi");
        tx.send(val).unwrap(); // val moved here
        println!("val is {}", val); // error: borrow of moved value
    });

    let received = rx.recv().unwrap();
    println!("Got: {}", received);
}
```

Sending Multiple Values and Seeing the Receiver Waiting

```rust
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in rx { // block main thread
        println!("Got: {}", received);
    }
}
```

Creating Multiple Producers by Cloning the Transmitter

```rust
// snip

let (tx, rx) = mpsc::channel();

let tx1 = mpsc::Sender::clone(&tx);

thread::spawn(move || {
    // snip
    for val in vals {
        tx1.send(val).unwrap();
        thread::sleep(Duration::from_secs(1));
    }
});

thread::spawn(move || {
    // snip
    for val in vals {
        tx.send(val).unwrap();
        // snip
    }
});

// snip
```

## Shared-State Concurrency

同channel相比，channel类似于single ownership，一旦发出去的数据就不能收回来，而Shared memory concurrency更像multiple ownership，多个线程可以在同一时间访问同一个内存地址

Using Mutexes to Allow Access to Data from One Thread at a Time

Mutex：mutual exclusion，有两条规则：

- 使用前先请求锁
- 使用完要释放锁

```rust
use std::sync::Mutex;

fn main() {
    let m = Mutex::new(5);

    {
        let mut num = m.lock().unwrap(); // 当另一个拥有锁的线程panic的时候lock会fail
        *num = 6;
    }

    println!("m = {:?}", m);
}
```

Mutex<T>是smart pointer, MutexGuard，被包裹在unwrap返回的LockResult中，实现了Deref和Drop trait，所以不需要手动释放锁

Multiple Ownership with Multiple Threads

Rc<T>在多线程下并不安全，锁的智能指针需要多个线程使用的时候，外层要包一个Arc<T>，atomic reference counter

Arc<T>有多余的开销，所以单线程下系统默认用Rc

```rust
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handlers = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handlers {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
```



Allowing Transference of Ownership Between Threads with `Send`

rust几乎所有类型都是`Send`，除了Rc<T>；所有由Send类型组成的复合类型也自动被标记成Send；几乎所有的primitive类型都是Send，除了`raw pointer`

Allowing Access from Multiple Threads with `Sync`

any type T is `Sync` if &T (a reference to T) is `Send`, meaning the reference can be sent safely to another thread.

Implementing `Send` and `Sync` Manually Is Unsafe

# Object Oriented Programming Features of Rust

```rust
pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        // ..
    }
}

struct SelectBox {
    width: u32,
    height: u32,
    options: Vec<String>,
}

impl Draw for selectBox {
    fn draw(&self) {
        // ..
    }
}

fn main() {
    let screen = Screen {
        components: vec![
            Box::new(SelectBox {
                width: 12,
                height: 12,
                options: vec![
                    String::from("aa"),
                    String::from("bb"),
                    String::from("cc"),
                ],
            }),
            Box::new(Button {
                width: 12,
                height: 12,
                label: String::from("OK"),
            }),
        ],
    };

    screen.run();
}
```

Trait Objects Perform Dynamic Dispatch，会损失一部分性能，但带来灵活性

A trait is object safe if all the methods defined in the trait have the following properties:

- The return type isn’t Self
- There are no generic type parameters

一个反面的例子是`Clone trait`:

```rust
pub trait Clone {
    fn clone(&self) -> Self;
}
```

举个例子，要完成以下功能：

1. A blog post starts as an empty draft.
2. When the draft is done, a review of the post is requested.
3. When the post is approved, it gets published.
4. Only published blog posts return content to print, so unapproved posts can’t accidentally be published.

main.rs
```rust
use blog::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("something");
    assert_eq!("", post.content());

    post.request_review();
    assert_eq!("", post.content());

    post.approve();
    assert_eq!("something", post.content());
}
```

lib.rs
```rust
pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn content(&self) -> &str {
        self.state.as_ref().unwrap().content(self)
    }

    pub fn request_review(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review());
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve())
        }
    }
}

trait State {
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;
    fn content<'a>(&self, post: &'a Post) -> &'a str {
        ""
    }
}

struct Draft {}

impl State for Draft {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview {})
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

struct PendingReview {}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published {})
    }
}

struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn content<'a>(&self, post: &'a Post) -> &'a str {
        &post.content
    }
}
```

上述的实现方案将业务逻辑都包含在了State里，Post不需要关心具体的业务逻辑，如果需求有变，再建一个新的struct实现State trait就好！

如果采用另一种写法，就需要在Post的content里加match，每次多一个类型就要多加一种arm，并且业务逻辑代码分散在各处，不好集中管理

这种实现方法还方便添加更多的功能，例如：

- Add a reject method that changes the post’s state from PendingReview back to Draft.
- Require two calls to approve before the state can be changed to Published.
- Allow users to add text content only when a post is in the Draft state. Hint: have the state object responsible for what might change about the content but not responsible for modifying the Post.

但是这种设计也有缺点，由于不同state之间强耦合，如果在PendingReview和Published中间加一个状态，比如Scheduled，那就需要改PendingReview的代码

还有一个缺点就是重复了一些逻辑代码，优化措施是可以在trait里添加默认方法，比如在trait定义中实现request_review和approve方法返回self，然而这又违反了object safety，因为State并不知道最终返回的对象具体是什么类型

缺点不仅这些，Post struct里也要重复实现request_review和approve方法；如果方法的种类有很多，就会有很多重复代码；解决方法是用`Macros`

其实还有另外一种思路来实现上面的功能：

Encoding States and Behavior as Types, implementing Transitions as Transformations into Different Types

```rust
pub struct Post {
    content: String,
}

pub struct DraftPost {
    content: String,
}

impl Post {
    pub fn new() -> DraftPost {
        DraftPost {
            content: String::new(),
        }
    }

    pub fn content(&self) -> &str {
        &self.content
    }
}

impl DraftPost {
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
        }
    }
}

pub struct PendingReviewPost {
    content: String,
}

impl PendingReviewPost {
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }
}
```

main.rs
```rust
use blog::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("something");

    let post = post.request_review();

    let post = post.approve();

    assert_eq!("something", post.content());
}
```

由不同类型来控制不同的业务逻辑，可以在编译期就隔绝所有可能发生的错误，这种方式比传统的oop更好

# Patterns and Matching

Patterns有两种类型：refutable（可能会失败的匹配，例如`if let Some(x) = val`）, irrefutable（可以适配任何数据的pattern，例如`let x = 5;`）

`Function parameters`, `let` statements, and `for` loops can only accept irrefutable patterns

The `if let` and `while let` expressions accept refutable and irrefutable patterns

所有的pattern用法：

Matching Literals

```rust
let x = 1;

match x {
    1 => println!("one"),
    2 => println!("two"),
    3 => println!("three"),
    _ => println!("anything"),
}
```

Matching Named Variables

```rust
let x = Some(5);
let y = 10;

match x {
    Some(50) => println!("Got 50"),
    Some(y) => println!("Matched, y = {:?}", y), // 这里的y是重新定义的局部变量
    _ => println!("Default case, x = {:?}", x),
}

println!("at the end: x = {:?}, y = {:?}", x, y);
```

Multiple Patterns

```rust
let x = 1;

match x {
    1 | 2 => println!("one or two"),
    3 => println!("three"),
    _ => println!("anything"),
}
```

Matching Ranges of Values with `..=`

```rust
let x = 5;

match x {
    1..=5 => println!("one through five"),
    _ => println!("something else"),
}
```

```rust
let x = 'c';

match x {
    'a'..='j' => println!("early ASCII letter"),
    'k'..='z' => println!("late ASCII letter"),
    _ => println!("something else"),
}
```

Destructuring Structs

```rust
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let p = Point { x: 0, y: 7 };

    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }
}
```

Destructuring Enums

```rust
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

fn main() {
    let msg = Message::ChangeColor(0, 160, 255);

    match msg {
        Message::Quit => {
            println!("The Quit variant has no data to destructure.")
        }
        Message::Move { x, y } => {
            println!(
                "Move in the x direction {} and in the y direction {}",
                x, y
            );
        }
        Message::Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(r, g, b) => println!(
            "Change the color to red {}, green {}, and blue {}",
            r, g, b
        ),
    }
}
```

Destructuring Nested Structs and Enums

```rust
enum Color {
    Rgb(i32, i32, i32),
    Hsv(i32, i32, i32),
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(Color),
}

fn main() {
    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));

    match msg {
        Message::ChangeColor(Color::Rgb(r, g, b)) => println!(
            "Change the color to red {}, green {}, and blue {}",
            r, g, b
        ),
        Message::ChangeColor(Color::Hsv(h, s, v)) => println!(
            "Change the color to hue {}, saturation {}, and value {}",
            h, s, v
        ),
        _ => (),
    }
}
```

Destructuring Structs and Tuples

```rust
let ((feet, inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
```

Ignoring an Entire Value with _

```rust
fn foo(_: i32, y: i32) {
    println!("This code only uses the y parameter: {}", y);
}

fn main() {
    foo(3, 4);
}
```

Ignoring Parts of a Value with a Nested _

```rust
let mut setting_value = Some(5);
let new_setting_value = Some(10);

match (setting_value, new_setting_value) {
    (Some(_), Some(_)) => {
        println!("Can't overwrite an existing customized value");
    }
    _ => {
        setting_value = new_setting_value;
    }
}

println!("setting is {:?}", setting_value);
```

Ignoring an Unused Variable by Starting Its Name with _

```rust
let s = Some(String::from("Hello!"));

if let Some(_s) = s {
    println!("found a string");
}

println!("{:?}", s); // error: s has been moved
```

这里代码会报错，因为虽然是匿名_s，但s还是被move到_s中，以下方法不会报错：

```rust
let s = Some(String::from("Hello!"));

if let Some(_) = s {
    println!("found a string");
}

println!("{:?}", s);
```

Ignoring Remaining Parts of a Value with ..

```rust
struct Point {
    x: i32,
    y: i32,
    z: i32,
}

let origin = Point { x: 0, y: 0, z: 0 };

match origin {
    Point { x, .. } => println!("x is {}", x),
}
```

```rust
fn main() {
    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, .., last) => {
            println!("Some numbers: {}, {}", first, last);
        }
    }
}
```

Extra Conditionals with Match Guards

```rust
let num = Some(4);

match num {
    Some(x) if x < 5 => println!("less than five: {}", x),
    Some(x) => println!("{}", x),
    None => (),
}
```

`@` Bindings

```rust
enum Message {
    Hello { id: i32 },
}

let msg = Message::Hello { id: 5 };

match msg {
    Message::Hello {
        id: id_variable @ 3..=7,
    } => println!("Found an id in range: {}", id_variable),
    Message::Hello { id: 10..=12 } => {
        println!("Found an id in another range")
    }
    Message::Hello { id } => println!("Found some other id: {}", id),
}
```

`@`可以攫取内部符合条件的数值并命名之

# Advanced Features

这一章讨论以下内容：

- Unsafe Rust: how to opt out of some of Rust’s guarantees and take responsibility for manually upholding those guarantees
- Advanced traits: associated types, default type parameters, fully qualified syntax, supertraits, and the newtype pattern in relation to traits
- Advanced types: more about the newtype pattern, type aliases, the never type, and dynamically sized types
- Advanced functions and closures: function pointers and returning closures
- Macros: ways to define code that defines more code at compile time

#  Unsafe rust

用unsafe包裹的代码块具有以下额外功能：

- Dereference a raw pointer
- Call an unsafe function or method
- Access or modify a mutable static variable
- Implement an unsafe trait
- Access fields of `union`s

注意，这里并不会关闭rust的安全检查，也不是说代码块里的代码不安全

## Dereferencing a Raw Pointer

`raw pointer`有两种，`*const T`和`*mut T`，分别是`immutable`和`mutable`

同`reference`相比，`raw pointer`有以下特点：

- Are allowed to ignore the borrowing rules by having both immutable and mutable pointers or multiple mutable pointers to the same location
- Aren’t guaranteed to point to valid memory
- Are allowed to be null
- Don’t implement any automatic cleanup

```rust
let address = 0x012345usize;
let r = address as *const i32;
```

```rust
let mut num = 5;

let r1 = &num as *const i32;
let r2 = &mut num as *mut i32;

unsafe {
    println!("r1 is {}", *r1);
    println!("r2 is {}", *r2);
}
```

这里同时有可读和可写的指针指向同一片内存地址，可能会引起data race；这么做的原因是为了和`C`语言交互

Creating a Safe Abstraction over Unsafe Code

```rust
let mut v = vec![1, 2, 3, 4, 5, 6];

let r = &mut v[..];

let (a, b) = r.split_at_mut(3);

assert_eq!(a, &mut [1, 2, 3]);
assert_eq!(b, &mut [4, 5, 6]);
```

如果如下实现会报错：

```rust
fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();

    assert!(mid <= len);

    (&mut slice[..mid], &mut slice[mid..]) // Error: borrow *slice as mutable more than once
}
```

修正：

```rust
use std::slice;

fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();
    let ptr = slice.as_mut_ptr();

    assert!(mid <= len);

    unsafe {
        (
            slice::from_raw_parts_mut(ptr, mid),
            slice::from_raw_parts_mut(ptr.add(mid), len - mid),
        )
    }
}
```

注意这里`split_at_mut`不需要加`unsafe`，我们可以在安全方法里添加`unsafe`代码块（但是前提是要保证该代码块不会出问题）

Using extern Functions to Call External Code

FFI: Foreign Function Interface

ABI: application binary interface, defines how to call the function at the assembly level

```rust
extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        println!("result: {}", abs(-3));
    }
}
```

也可以对外暴露rust的代码

```rust
#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Just called a Rust function from C!");
}
```

Mangling: when a compiler changes the name we’ve given a function to a different name that contains more information for other parts of the compilation process to consume but is less human readable

Accessing or Modifying a Mutable Static Variable

全局变量：

```rust
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc;
    }
}

fn main() {
    add_to_count(3);

    unsafe {
        println!("COUNTER: {}", COUNTER);
    }
}
```

Implementing an Unsafe Trait

```rust
unsafe trait Foo {
    // snip
}

unsafe impl Foo for i32 {
    // snip
}
```

Accessing Fields of a Union

union和struct类似，主要是为了同C交互

# Advanced Traits

Specifying Placeholder Types in Trait Definitions with Associated Types

```rust
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Optoin<Self::Item>;
}
```

Default Generic Type Parameters and Operator Overloading

```rust
use std::ops::Add;

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {
    type Output = Point;

    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

fn main() {
    assert_eq!(
        Point { x: 1, y: 0 } + Point { x: 2, y: 3 },
        Point { x: 3, y: 3 }
    )
}
```

Add trait的定义如下：

```rust
trait Add<RHS=Self> {
    type Output;

    fn add(self, rhs: RHS) -> Self::Output;
}
```

```rust
use std::ops::Add;

struct Millimeters(u32);
struct Meters(u32);

impl Add<Meter> for Millimeters {
    type Output = Millimeters;

    fn add(self, other: Meters) -> Millimeters {
        Millimeters(self.0 + (other.0 * 1000))
    }
}
```

Fully Qualified Syntax for Disambiguation: Calling Methods with the Same Name

```rust
trait Pilot {
    fn fly(&self);
}

trait Wizard {
    fn fly(&self);
}

struct Human;

impl Pilot for Human {
    fn fly(&self) {
        println!("This is your captain speaking.");
    }
}

impl Wizard for Human {
    fn fly(&self) {
        println!("Up!");
    }
}

impl Human {
    fn fly(&self) {
        println!("*waving arms furiously*");
    }
}

fn main() {
    let person = Human;
    Pilot::fly(&person);
    Wizard::fly(&person);
    person.fly();
}
```

然而，当方法的入参不带self的时候就会出现歧义，比如：

```rust
trait Animal {
    fn baby_name() -> String;
}

struct Dog;

impl Dog {
    fn baby_name() -> String {
        String::from("Spot")
    }
}

impl Animal for Dog {
    fn baby_name() -> String {
        String::from("puppy")
    }
}

fn main() {
    println!("A baby dog is called a {}", Dog::baby_name()); // Ok
    println!("A baby dog is called a {}", Animal::baby_name()); // Error: type annotations needed
    println!("A baby dog is called a {}", <Dog as Animal>::baby_name()); // Ok
}
```

Using Supertraits to Require One Trait’s Functionality Within Another Trait

```rust
use std::fmt;

trait OutlinePrint: fmt::Display {
    fn outline_print(&self) {
        let output = self.to_string();
        let len = output.len();
        println!("{}", "*".repeat(len + 4));
        println!("{}", output);
    }
}

struct Point {
    x: i32,
    y: i32,
}

impl OutlinePrint for Point {}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}
```

Using the Newtype Pattern to Implement External Traits on External Types

rust不允许给外部crate的struct实现外部crate的trait，例如给Vec实现Display trait，但是如果用`newtype`可以绕过此规则

具体方法是给外部struct套一层由单元素的tuple组成的struct：

```rust
use std::fmt;

struct Wrapper(Vec<String>);

impl fmt::Display for Wrapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}]", self.0.join(", "))
    }
}

fn main() {
    let w = Wrapper(vec![String::from("hello"), String::from("world")]);
    println!("w = {}", w);
}
```

这样做有一个坏处，就是无法直接获取内层struct的所有方法，要通过操作self.0；有个解决办法是实现Deref方法；但是如果只想实现部分方法就只能一个一个实现

# Advanced Types

Using the Newtype Pattern for Type Safety and Abstraction

用包裹成新类型的方法实现封装和抽象

Creating Type Synonyms with Type Aliases

```rust
type Kilometers = i32;
```

The Never Type that Never Returns

```rust
fn bar() -> ! {
    // snip
}
```

```rust
let guess: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
};
```

这里第二个arm返回的就是`!`

### Dynamically Sized Types and the `Sized` Trait

Rust需要知道某种类型需要分配多少内存，并且所有这种类型的数据都要使用同等大小的内存，例如`&str`包含两部分呢，实际内存的地址和数据的长度；所以处理不定长度的数据，需要把实际数据隐藏在指针的背后

事实上，在引用trait object的时候就是如此，需要加上数据的引用才行

`Sized trait`就是用来描述一个类型在编译期的长度是否固定，所有在编译期长度已知的类型都自动被实现了`Sized trait`

未知是否满足Sized trait的generic type写法如下：

```rust
fn generic<T: ?Sized>(t: &T) {
    // snip
}
```

该写法**只针对**Sized trait

# Advanced Functions and Closures

Function Pointers

```rust
fn add_one(x: i32) -> i32 {
    x + 1
}

fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}

fn main() {
    let answer = do_twice(add_one, 5);
    
    println!("The answer is: {}", answer);
}
```

```rust
enum Status {
    Value(u32),
    Stop,
}

let list: Vec<Status> = (0u32..20).map(Status::Value).collect();
```

这里Status::Value也是function pointer

Returning Closures

返回的closure需要用Box包裹

```rust
fn returns_closure() -> Box<dyn Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}
```

# Macros

The term macro refers to a family of features in Rust: declarative macros with macro_rules! and three kinds of procedural macros:

- Custom #[derive] macros that specify code added with the derive attribute used on structs and enums
- Attribute-like macros that define custom attributes usable on any item
- Function-like macros that look like function calls but operate on the tokens specified as their argument

## Macro和Function的区别

本质上，macro就是产生代码的代码；macro比function有更多的power；macro的入参数量不必固定；macro产生代码在编译之前，而function要在运行时执行；macro的语法更晦涩难懂

## Declarative Macros with `macro_rules!` for General Metaprogramming

有点像match的概念

```rust
#[macro_export]
macro_rules! vec {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}
```

调取vec![1, 2, 3]，会生成如下代码：

```rust
{
    let mut temp_vec = Vec::new();
    temp_vec.push(1);
    temp_vec.push(2);
    temp_vec.push(3);
    temp_vec
}
```

`macro_rules!`有一些edge cases还没有解决，后面版本更新会deprecate掉

### Procedural Macros for Generating Code from Attributes

Procedural macros accept some code as an input, operate on that code, and produce some code as an output rather than matching against patterns and replacing the code with other code as declarative macros do.

# Final Project: Building a Multithreaded Web Server

main.rs
```rust
use std::thread;
use std::time::Duration;
use std::fs;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;

use hello::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming().take(2) {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }

    println!("Shutting down.");
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    stream.read(&mut buffer).unwrap();

    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";

    let (status_line, filename) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else if buffer.starts_with(sleep) {
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK\r\n\r\n", "hello.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
    };

    let contents = fs::read_to_string(filename).unwrap();
    
    let response = format!("{}{}", status_line, contents);

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}
```

lib.rs
```rust
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::sync::mpsc;

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

type Job = Box<dyn FnOnce() + Send + 'static>;

impl ThreadPool {
    /// Create a new ThreadPool.
    /// 
    /// The size is the number of threads in the pool.
    /// 
    /// # Panics
    /// 
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        for _ in &self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");
        
        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let message = receiver.lock().unwrap().recv().unwrap();

            match message {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);
        
                    job();
                }
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }

        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}

enum Message {
    NewJob(Job),
    Terminate,
}
```