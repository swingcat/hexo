---
title: go flow control
date: 2018-04-20 17:46:16
tags: go
---
# for循环

```go
package main

import "fmt"

func main() {
    sum := 0
    for i := 0; i < 10; i++ {
        sum += i
    }
    fmt.Println(sum)
}
```

循环的第一个和第三个声明也可以省略：

```go
package main

import "fmt"

func main() {
    sum := 1
    for ; sum < 1000; {
        sum += sum
    }
    fmt.Println(sum)
}
```

也可以把`for`当`while`用:

```go
package main

import "fmt"

func main() {
    sum := 1
    for sum < 1000 {
        sum += sum
    }
    fmt.Println(sum)
}
```

如果想要制造无限循环，只要省略所有参数即可：

```go
package main

func main() {
    for {
    }
}
```

# if

`if`和`for`类似，不需要用小括号

```go
package main

import (
    "fmt"
    "math"
)

func sqrt(x float64) string {
    if x < 0 {
        return sqrt(-x) + "i"
    }
    return fmt.Sprint(math.Sqrt(x))
}

func main() {
    fmt.Println(sqrt(2), sqrt(-4))
}
```

`if`也有简写形式，简写形式内定义的变量的作用域仅限于该if语句，包括后面接的else语句

```go
package main

import (
    "fmt"
    "math"
)

func pow(x, n, lim float64) float64 {
    if v := math.Pow(x, n); v < lim {
        return v
    }
    return lim
}

func main() {
    fmt.Println(
        pow(3, 2, 10),
        pow(3, 3, 20)
    )
}
```

# switch

```go
package main

import (
    "fmt"
    "runtime"
)

func main() {
    fmt.Print("Go runs on ")
    switch os := runtime.GOOS; os {
        case "darwin":
            fmt.Println("OS X.")
        case "linux":
            fmt.Println("Linux.")
        default:
            fmt.Printf("%s", os)
    }
}
```

当case值为True，跳出循环

当需要判断多个条件，只要有一个为真，其余判断都不需要执行时，实现方法如下：

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    t := time.Now()
    switch {
        case t.Hour() < 12:
            fmt.Println("good morning")
        case t.Hour() < 17:
            fmt.Println("good afternoon")
        default:
            fmt.Println("good evening")
    }
}
```

# defer

`defer`声明的函数在外层函数执行完毕后再执行，虽然参数是即时执行

```go
defer fmt.Println("world")
fmt.Println("hello")
```

`defer`放在堆里，最后进堆里的先出

```go
fmt.Println("counting")

for i := 0; i < 10; i++ {
    defer fmt.Println(i)
}

fmt.Println("done")
```