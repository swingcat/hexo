---
title: go-并发
date: 2018-04-27 09:22:46
tags: go
---
# Goroutines

`goroutine`是go运行时的一个轻量级线程

```go
go f(x, y, z)
```

f, x, y, z的evaluation和f的execution是在不同的goroutine下完成的

goroutine是在相同的地址空间内运行的，所以访问共享内存必须是同步访问

```go
package main

import {
    "fmt"
    "time"
}

func say(s string) {
    for i := 0; i < 5; i++ {
        time.Sleep(1000 * time.Millisecond)
        fmt.Println(s)
    }
}

func main() {
    go say("world")
    say("hello")
}
```

# Channels

`channel`就是能够接收和发送数据的导管，通过`<-`符号连接

```go
ch <- v
v := <-ch
```

在使用之前要先创建

```go
ch := make(chan int)
```

默认情况下，当另一边准备完毕了才会接收block，这样保证了代码同步执行

```go
func sum(s []int, c chan int) {
    sum := 0
    for _, v := range s {
        sum += v
    }
    c <- sum
}

func main() {
    s := []int{1, 2, 3, 4, 5, 6}
    c := make(chan int)

    go sum(s[:len(s)/2], c)
    go sum(s[len(s)/2:], c)

    x, y := <-c, <-c

    fmt.Println(x, y, x+y)
}
```

# Buffered Channels

可以创建缓冲频道，当频道长度超过一定限制时才会发送

```go
package main

import (
    "fmt"
    "time"
)

var c = make(chan int, 5)

func main() {
    go worker(1)
    for i := 0; i < 10; i++ {
        c <- i
        fmt.Println(i)
    }
}

func worker(id int) {
    for {
        _ = <-c
        time.Sleep(time.Second)
    }
}
```

# 通道的遍历和关闭

发送方可以关闭通道，接收方可以通过这种方法测试通道是否关闭：

```go
v, ok := <-ch
```

遍历通道的方法为`for i := range c`，直到通道关闭

```go
func fibonacci(n int, c chan int) {
    x, y := 0, 1
    for i := 0; i < n; i++ {
        c <- x
        x, y = y, x+y
    }
    close(c)
}

func main() {
    c := make(chan int, 20)
    go fibonacci(cap(c), c)
    for i := range c {
        fmt.Println(i)
    }
}
```

# select

`select`的作用是，让一个`goroutine`等待多个操作通道的指令

在通道操作之前是待命状态，直到有一个通道操作成功，如果多个通道操作同时成功，则随机选择一个执行

```go
func fibonacci(c, quit chan int) {
    x, y := 0, 1
    for {
        select {
        case c <- x:
            x, y = y, x+y
        case <-quit:
            fmt.Println("quit")
            return
        }
    }
}

func main() {
    c := make(chan int)
    quit := make(chan int)
    go func() {
        for i := 0; i < 10; i++ {
            fmt.Println(<-c)
        }
        quit <- 0
    }()
    fibonacci(c, quit)
}
```

当没有通道操作时执行`default`选项

```go
func main() {
    tick := time.Tick(100 * time.Millisecond)
    boom := time.After(500 * time.Millisecond)
    for {
        select {
        case <-tick:
            fmt.Println("tick")
        case <-boom:
            fmt.Println("boom")
            return
        default:
            fmt.Println(".")
            time.Sleep(50 * time.Millisecond)
        }
    }
}
```

# sync.Mutex

channel是用来通信的，但是如果两个goroutine不需要通信，只需要共享内存呢？

`mutual exclusion`的简写是`mutex`，go的标准库里提供了sync.Mutex以及它的两个方法：`Lock`和`Unlock`

```go
package main

import (
    "fmt"
    "sync"
    "time"
)

type SafeCounter struct {
    v map[string]int
    mux sync.Mutex
}

func (c *SafeCounter) Inc(key string) {
    c.mux.Lock()
    c.v[key]++
    c.mux.Unlock()
}

func (c *SafeCounter) Value(key string) int {
    c.mux.Lock()
    defer c.mux.Unlock()
    return c.v[key]
}

func main() {
    c := SafeCounter{v: make(map[string]int)}
    for i := 0; i < 1000; i++ {
        go c.Inc("somekey")
    }

    time.Sleep(time.Second)
    fmt.Println(c.Value("somekey"))
}
```