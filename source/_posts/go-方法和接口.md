---
title: 方法和接口
date: 2018-04-25 16:12:25
tags: go
---
# method

`go`没有类，但是可以定义某类型变量的方法，只需要在定义方法的时候上传一个`receiver`

```go
type Vertex struct {
    X, Y float64
}

func (v Vertex) Abs() float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
    v := Vertex{3, 4}
    fmt.Println(v.Abs())
}
```

还可以使用非`struct`类型的数据，注意函数名首字母大写

```go
type MyFloat float64

func (f MyFloat) Abs() float64 {
    if f < 0 {
        return float64(-f)
    }
    return float64(f)
}

func main() {
    f := MyFloat(-math.Sqrt2)
    fmt.Println(f.Abs())
}
```

若`receiver`传入的不是指针，则该变量是通过**复制**的方法传入的，要改变原变量，则要传入指针

```go
type Vertex struct {
    X, Y float64
}

func (v Vertex) Abs() float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *Vertex) Scale(f float64) {
    v.X = v.X * f
    v.Y = v.Y * f
}

func main() {
    v := Vertex{3, 4}
    v.Scale(10)
    fmt.Println(v.Abs())
}
```

或者函数可以直接传入指针

```go
type Vertex struct {
    X, Y float64
}

func Abs(v Vertex) float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func Scale(v *Vertex, f float64) {
    v.X = v.X * f
    v.Y = v.Y * f
}

func main() {
    v := Vertex{3, 4}
    Scale(&v, 10)
    fmt.Println(Abs(v))
}
```

再或者这样用

```go
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func ScaleFunc(v *Vertex, f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	v.Scale(2)
	ScaleFunc(&v, 10)

	p := &Vertex{4, 3}
	p.Scale(3)
	ScaleFunc(p, 8)

	fmt.Println(v, p)
}
```

带`receiver`的`method`，只要`receiver`声明了传入参数类型（值或者指针），不论是用值或者指针调该`method`，都按`receiver`的声明来

```go
type Vertex struct {
    X float64
}

func (v Vertex) Scale1(f float64) {
    v.X = v.X * f
}

func (v *Vertex) Scale2(f float64) {
    v.X = v.X * f
}

func main() {
    v1 := Vertex{1}
    v2 := Vertex{1}
    p1 := &Vertex{1}
    p2 := &Vertex{1}
    v1.Scale1(10)
    v2.Scale2(10)
    p1.Scale1(10)
    p2.Scale2(10)
    fmt.Println(v1)
    fmt.Println(v2)
    fmt.Println(p1)
    fmt.Println(p2)
}
```

但是`function`不可以

```go
AbsFunc(v) // OK
AbsFunc(&v) // Compile error
```

使用指针`receiver`有两点好处：

- 函数内可以改变传入参数的值
- 避免每次调用该方法都要复制原值，比如传入的值是一个非常大的数据

# interface

`interface`类型就是`method`的集合

该接口类型的值可以被任何实现该接口方法的值所赋值，隐式实现接口方法(没有`implements`关键词)

```go
type Abser interface {
    Abs() float64
}

func main() {
    var a Abser
    f := MyFloat(-math.Sqrt2)
    v := Vertex{3, 4}

    a = f
    a = &v

    fmt.Println(a.Abs())
}

type MyFloat float64

func (f MyFloat) Abs() float64 {
    if f < 0 {
        return float64(-f)
    }
    return float64(f)
}

type Vertex struct {
    X, Y float64
}

func (v *Vertex) Abs() float64 {
    return math.Sqrt(v.X*v.X + v.Y*v.Y)
}
```

`interface`的值可以认为是一个`(value, type)`的`tuple`

```go
type I interface {
    M()
}

type T struct {
    S string
}

func (t *T) M() {
    fmt.Println(t.S)
}

type F float64

func (f F) M() {
    fmt.Println(f)
}

func main() {
    var i I

    i = &T{"hello"}
    describe(i)
    i.M()

    i = F(math.Pi)
    describe(i)
    i.M()
}

func describe(i I) {
    fmt.Printf("(%v, %T)\n", i, i)
}
```

如果`interface`的赋值没有初始化的话，传入该`interface`方法的参数为`nil`

```go
type I interface {
    M()
}
type T struct(
    X string
)
func (t T) M() {
    fmt.Println(t.X)
}
var i I
var t T
i = t
i.M()
```

如果`interface`没有赋值就调用方法的话会报错

```go
var i I
i.M()
```

空`interface`被用来处理未知类型的值

```go
func main() {
    var i interface{}
    describe(i)
    
    i = 42
    describe(i)
    
    i = "hello"
    describe(i)
}

func describe(i interface{}) {
    fmt.Printf("(%v, %T)\n", i, i)
}
```

# Type assertions

验证`interface`类型以及赋值的方法是`t, ok = i.(T)`，如果i的值不是T类型的话`ok`为`false`

```go
var i interface{} = "hello"

s := i.(string) // s == "hello"
s, ok := i.(string) // 注意这里的:=
f, ok := i.(float64) // (0, false)
f = i.(float64) // panic，注意这里的=
```

# Type switches

用来验证`interface`数据的类型，使用方法和`switch`一致

```go
func do(i interface{}) {
    switch v := i.(type) {
        case int:
            fmt.Printf("Twice %v is %v\n", v, v*2)
        case string:
            fmt.Printf("%q is %v bytes long\n", v, len(V))
        default:
            fmt.Printf("I don't know about type %T\n", v)
    }
}

func main() {
    do(21)
    do("hello")
    do(true)
}
```

# Stringers

最常见的接口是`fmt`定义的`Stringer`

```go
type Stringer interface {
    String() string
}
```

`fmt`还有很多其他包用这个`interface`打印值

```go
type Person struct {
    Name string
    Age int
}

func (p Person) String() string {
    return fmt.Sprintf("%v (%v years)", p.Name, p.Age) // 注意是Sprintf不是Printf
}

func main() {
    a := Person{"lewis", 27}
    b := Person{"chen", 26}
    fmt.Println(a, b)
}
```

# Errors

`error`类型和`fmt.Stringer`接口类似

```go
type error interface {
    Error() string
}
```

函数经常会返回错误信息，并需要判断错误是否为`nil`

```go
package main

import (
    "fmt"
    "time"
)

type MyError struct {
    When time.Time
    What string
}

func (e *MyError) Error() string {
    return fmt.Sprintf("at %v, %s", e.When, e.What)
}

func run() error {
    return &MyError{
        time.Now(),
        "it did't work",
    }
}

func main() {
    if err := run(); err != nil {
        fmt.Println(err)
    }
}
```

# Readers

io包提供了接口`io.Reader`,具有`Read`方法: `func (T) Read(b []byte) (n int, err error)`

当读操作结束时返回`io.EOF`错误

```go
package main

import (
    "fmt"
    "io"
    "strings"
)

func main() {
    r := strings.NewReader("Hello, Reader")

    b := make([]byte, 8)
    for {
        n, err := r.Read(b)
        fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
        fmt.Printf("b[:n] = %q\n", b[:n])
        if err == io.EOF {
            break
        }
    }
}
```

# Images

```go
package image

type Image interface {
    ColorModel() color.Model
    Bounds() image.Rectangle
    At(x, y int) color.Color
}
```

```go
package main

import (
	"fmt"
	"image"
)

func main() {
	m := image.NewRGBA(image.Rect(0, 0, 100, 100))
	fmt.Println(m.Bounds())
	fmt.Println(m.At(0, 0).RGBA())
}
```